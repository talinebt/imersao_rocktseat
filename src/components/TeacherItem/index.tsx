import React from 'react';

import whatsappIcon from '../../assets/images/icons/whatsapp.svg';

import './styles.css';

function TeacherItem() {
  return (
    <article className="teacher-item">
      <header>
        <img
          src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/5993503/avatar.png?width=90"
          alt="Taline"
        />
        <div>
          <strong>Taline Rodrigues</strong>
          <span>FullStack</span>
        </div>
      </header>
      <p>
        Entusiasta das melhores tecnologias .
        <br /> <br />
        Formada em Análise e Desenvolvimento de Sistemas, possui 8 anos de experiência na área de suporte e atendimento à cliente. Pela paixão em desenvolvimento Web, vem se especializando em programação Web e Designer Digital. Apaixonada pelo mundo dos games e séries, gosta estar rodeada de amigos compartilhando risadas. Viciada em conhecimento, explora lugares e culturas diferentes. Desenvolvedora full stack, DBA e arquiteta de software na Pixel Create.
      </p>

      <footer>
        <p>
          Preço/hora
          <strong>R$ 100,00</strong>
        </p>
        <button type="button">
          <img src={whatsappIcon} alt="Whatsapp" />
          Entrar em contato
        </button>
      </footer>
    </article>
  );
}

export default TeacherItem;